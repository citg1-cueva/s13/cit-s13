package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userServices;

    @RequestMapping(value ="/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody User user){
        userServices.createUser(user);
        return new ResponseEntity<>("User created successfully", HttpStatus.CREATED);
    }
    @RequestMapping(value ="/users", method = RequestMethod.GET)
    public ArrayList<User> getallUser(){

        return userServices.getallUsers();
    }
    @RequestMapping(value ="/getusers", method = RequestMethod.GET)
    public Optional<User> getUser(@RequestParam Long id){

        return userServices.getUser(id);
    }
    @RequestMapping(value ="/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable Long id){

        return userServices.deleteUser(id);
    }
    @RequestMapping(value ="/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateUser(@PathVariable Long id,@RequestBody User updated){
        return userServices.updateUser(id, updated);
    }

}
