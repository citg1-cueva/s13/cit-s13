package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.services.PostService;
import com.zuitt.discussion.services.PostServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    private PostService postServices;

    @RequestMapping(value ="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post post){
        postServices.createPost(post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @PostMapping("/createPost")
    public void createPost1(@RequestBody Post post) throws Exception{
        postServices.createPost(post);
    }

    @RequestMapping(value ="/posts", method = RequestMethod.GET)
    public ArrayList<Post> getallPost(){

        return postServices.getallPost();
    }

    @RequestMapping(value ="/getposts", method = RequestMethod.GET)
    public Optional<Post> getPost(@RequestParam long id){
        return postServices.getPost(id);
    }

    @RequestMapping(value ="/posts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletePost(@PathVariable Long id){
        return postServices.deletePost(id);
    }
    @RequestMapping(value ="/posts/{id}", method = RequestMethod.PUT)
    public ResponseEntity updatePost(@PathVariable Long id,@RequestBody Post updated){
        return postServices.updatePost(id, updated);
    }
}
