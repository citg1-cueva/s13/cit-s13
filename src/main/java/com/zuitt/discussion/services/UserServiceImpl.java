package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;


    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.empty();
    }

    @Override
    public void createUser(User user) {
        userRepository.save(user);
    }

    @Override
    public ArrayList<User> getallUsers() {
        return (ArrayList<User>) userRepository.findAll();
    }

    @Override
    public Optional<User> getUser(long id) {
        return userRepository.findById(id);
    }

    @Override
    public ResponseEntity deleteUser(Long id) {
        userRepository.deleteById(id);
        return new ResponseEntity<>("user remove", HttpStatus.OK);
    }

    @Override
    public ResponseEntity updateUser(Long id, User updated) {
        User user = new User();
        user = userRepository.findById(id).get();
        if(user != null){
            user.setPassword(updated.getPassword());
            user.setUsername(updated.getUsername());
            userRepository.save(user);
            return new ResponseEntity<>("post updated", HttpStatus.OK);

        }

        return new ResponseEntity<>("post not updated", HttpStatus.OK);
    }
}
