package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class PostServiceImpl  implements PostService{
    @Autowired
    private PostRepository postRepository;
    public void createPost(Post post){
        postRepository.save(post);
    }
    public ArrayList<Post> getallPost(){
        return (ArrayList<Post>) postRepository.findAll();
    }
    public Optional<Post> getPost(long id){
        return postRepository.findById(id);
    }

    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("post delted", HttpStatus.OK);
    }
    public ResponseEntity updatePost(Long id, Post updated) {
        Post post = new Post();
        post = postRepository.findById(id).get();
        if(post != null){
            post.setTitle(updated.getTitle());
            post.setContent(updated.getContent());
            postRepository.save(post);
            return new ResponseEntity<>("post updated", HttpStatus.OK);

        }

        return new ResponseEntity<>("post not updated", HttpStatus.OK);
    }
}
