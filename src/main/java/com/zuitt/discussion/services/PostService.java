package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Optional;


public interface PostService{

    void createPost(Post post);
    ArrayList<Post> getallPost();

    Optional<Post> getPost(long id);

    ResponseEntity deletePost(Long id);

    ResponseEntity updatePost(Long id, Post updated);

}
